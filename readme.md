# metamuffin's blog

My personal blog's source code.

## Ideas

## Todo (blog-tool)

- table parser
- table styling
- markdown images
- parser error handling

## License

- For `./code`: GNU-AGPL-3.0-only
- For `./content`: CC-BY-ND-4.0
- See `./COPYING`
