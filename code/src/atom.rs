use crate::{get_articles, html::escape, Args, ArticleMeta, BLOG_BASE};
use std::process::{Command, Stdio};

pub fn generate_atom(args: &Args) -> String {
    let entries = get_articles(&args.root.as_ref().unwrap())
        .iter()
        .map(
            |ArticleMeta {
                 title,
                 date,
                 canonical_name,
                 ..
             }| {
                let title = escape(title);
                let datetime = iso8601::DateTime {
                    date: date.clone(),
                    time: iso8601::Time {
                        hour: 0,
                        minute: 0,
                        second: 0,
                        millisecond: 0,
                        tz_offset_hours: 0,
                        tz_offset_minutes: 0,
                    },
                };
                format!(
                    r#"
    <entry>
        <title>{title}</title>
        <link href="{BLOG_BASE}/{canonical_name}" />
        <id>tag:metamuffin.org,{date},{title}</id>
        <published>{datetime}</published>
        <summary>N/A</summary>
        <author>
            <name>metamuffin</name>
            <email>metamuffin@disroot.org</email>
        </author>
    </entry>"#
                )
            },
        )
        .collect::<Vec<_>>();
    format!(
        r#"<?xml version="1.0" encoding="utf-8"?>
    <feed xmlns="http://www.w3.org/2005/Atom">
	<title>metamuffin's blog</title>
	<subtitle>where they post pointless stuff</subtitle>
	<link href="{BLOG_BASE}/feed.atom" rel="self" />
	<link href="{BLOG_BASE}/" />
	<id>urn:uuid:3cf2b704-3d94-4f1f-b194-42798ab5b47c</id>
	<updated>{}</updated>
    <author>
        <name>metamuffin</name>
        <email>metamuffin@disroot.org</email>
    </author>
    {}
    </feed>
    "#,
        now_rfc3339(),
        entries.join("\n")
    )
}

fn now_rfc3339() -> String {
    String::from_utf8(
        Command::new("/usr/bin/date")
            .arg("--iso-8601=minutes")
            .stdout(Stdio::piped())
            .output()
            .unwrap()
            .stdout,
    )
    .unwrap()
}
