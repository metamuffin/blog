use crate::{
    article_metadata, file_history, get_articles,
    markdown::{self},
    ArticleMeta,
};
use laby::{frag, html, iter, li, Render};
use std::fs::read_to_string;

pub fn scaffold(title: String, body: impl Render) -> impl Render {
    html!(
        head!(
            meta!(charset = "UTF-8"),
            link!(rel = "stylesheet", href = "./style.css"),
            title!(format!("{} - metamuffin's blog", title))
        ),
        body!(
            nav!(
                h2!("metamuffin's blog"),
                a!(href = "./index", "index"),
                " ",
                a!(href = "./feed.atom", "atom")
            ),
            article!(body),
            footer!(p!("written by metamuffin, licensed under CC-BY-ND-4.0"))
        )
    )
}

pub fn article(path: String) -> impl Render {
    scaffold(
        article_metadata(path.clone().into()).title,
        frag!(
            laby::raw!(markdown::render(&read_to_string(&path).unwrap())),
            hr!(),
            p!(
                "changes to this article (as recorded with git; visible ",
                a!(
                    href = format!(
                        "https://codeberg.org/metamuffin/blog/commits/branch/master/content/{path}"
                    ),
                    "on codeberg"
                ),
                ")"
            ),
            pre!(file_history(&path))
        ),
    )
}

pub fn index(root: &str) -> impl Render {
    scaffold(
        "index".to_string(),
        frag!(
            p!(
                "This is my personal blog. The sources (including all the tooling) are available ",
                a!(href = "https://codeberg.org/metamuffin/blog", "on codeberg")
            ),
            ul!(iter!({
                get_articles(&root).into_iter().map(
                    |ArticleMeta {
                         canonical_name,
                         date,
                         title,
                         ..
                     }| {
                        li!(
                            date.to_string(),
                            ": ",
                            a!(href = format!("./{canonical_name}",), title)
                        )
                    },
                )
            }))
        ),
    )
}

pub fn escape(text: &str) -> String {
    text.replace("&", "&amp;")
        .replace("<", "&lt;")
        .replace(">", "&gt;")
        .replace("'", "&#8217;")
        .replace("\"", "&quot;")
}
