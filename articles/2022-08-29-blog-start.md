# Starting my blog

This will be the place where I will post short articles about my thoughts and projects in the future, whenever I feel like it.

A list of topics of which I already have an idea in mind are:

- The tools I used to create this blog
- My dream operating system (maybe even logs about me trying (aka failing) to implement it)
- In search of a perfect text editor
