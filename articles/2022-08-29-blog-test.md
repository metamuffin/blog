# Blog test

Hello world!

## heading

_italic_ **bold** `code` [Link](https://metamuffin.org/)

### sub heading

Look at this code here.

```rs
#[derive(Default)]
struct Impossible<T> {
    something: HashMap<usize, T>,
    //    ^- look here, a keyword! --the grammar regex
    itself: Impossible,
}

pub async fn useless(s: Box<dyn Write>) -> impl Future<usize> {
    todo!() // this panics
}
fn main() { 
    1 + "blub"
    for (x, y) in [(1,2),(3,4)] {
        println!("{x} and {y}");
        Impossible::default();
    }
}
```

As you can see, its pointless i.e. no points

- list
- list
- list

1. first this
2. then that
3. done
