# Some Thoughts on Programming Language Design

This is a collection of ideas to look at when inventing new langauges.

## Other Ideas

- The Language pushes abstraction to the limit by not noting any
  hardware-related issues like memory-allocations, parallelism, heterogenous
  computer architecture (CPU, GPU, …)
  - requires a very "intellegent" compiler and a way to describe unknowns like
    possible inputs, etc. in the language itself
- Start with assembly but add a very flexible macro system

## Type System

```diff
# Haskell
data LinkedList a = Nil | Cons a (Box (LinkedList a))
data Test = Empty | Blub Int | State { x :: Int, y :: Int }
# Rust
enum LinkedList<T> { Nil, Cons(T, LinkedList<T>) }
```

## Memory Management

- **Drop when out-of-scope**
- Garbage collections
- Reference counting

## Compile-time logic

- Annotation when calling function to be run as-far-as-possible at comptime

```diff
fn format(template: String, args: [String]) -> String {
    template.replace("@", (match, i) => args[i])
}

fun add(x, y) x + y

fun main() print(format!("@ ist @; @", ["1+1", 1+1, x]))
# should expand to
fun main() print("1+1 ist 2; " ~ x))
```

## Examples

### Fizz-Buzz

```diff
for (n in 0..100) {
    if (n % (3*5) == 0) print("FizzBuzz")
    else if (n % 3 == 0) print("Fizz")
    else if (n % 5 == 0) print("Buzz")
    else print(n)
}


if (true) x = 1
if (true) { x = 1 }
```

```
f(x) = 10 + g(x)
f x = 10 + g x

main = {

}
```
